serviceDir=""
group=""
envFile=""

# Sets the current working directory
function configureServiceDir() {
    if [ ! -d "./codedeploy" ]; then
      echo "Changing directory to service dir"
      cd /var/piv/services/$SERVICE_DIR
    fi;

    serviceDir=`pwd`
    echo "Current dir: $serviceDir"
}

# Determines the name of the current deployment group
function configureDeploymentGroup() {
  if [ ! -z "$DEPLOYMENT_GROUP_NAME" ]; then
    group=$DEPLOYMENT_GROUP_NAME
  else

    echo "Deployment group variable not available. Using argument param"
    if [ -z "$1" ]; then
      echo "Could not obtain deployment group"
      exit 1;
    fi;

    group=$1
  fi

  echo "Deployment group set: $group"
}

function loadConfigFile() {
  # config file
  envFile="$serviceDir/codedeploy/config/$group.ini"

  if [ ! -f "$envFile" ]; then
    echo "Could not find any matching config files for $group."
    exit 1;
  fi

  echo "Using deployment group: $group, config file: $envFile"
}
