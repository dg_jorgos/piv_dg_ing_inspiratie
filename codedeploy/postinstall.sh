# Include service vars defs
. $(dirname $0)/vars.sh

# Include functions (custom)
. $(dirname $0)/functions.sh

configureServiceDir

configureDeploymentGroup

loadConfigFile

. $(dirname $0)/hubot_say.sh "$group : Deploying $APPLICATION_NAME"

if [[ "$group" != "ftp-"* ]]
then
    echo "Configuring apache"
    php codedeploy/replacevars.php $envFile codedeploy/etc/conf.d/service.conf > codedeploy/etc/conf.d/service.conf-tpl
    cp -f codedeploy/etc/conf.d/service.conf-tpl /etc/httpd/conf.d/$APPLICATION_NAME.conf
else
    echo "Installing cron files"
    for filename in codedeploy/etc/cron.d/*.cron; do
        cp -f $filename "/etc/cron.d/${SERVICE_DIR}_${filename##*/}"
    done
fi

echo "Clearing cache"
rm -rf /var/piv/cache/*

# cloudwatch logs
# log rotate

echo "Restarting apache"
service httpd restart
