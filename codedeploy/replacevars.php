<?php

$template = trim($argv[1]);
$config = trim($argv[2]);

if (!file_exists($template) || !file_exists($config)) {
    echo 'Missing input file for replacement';
    exit;
}

$configContents = parse_ini_file($template);
$templateContents = file_get_contents($config);
echo str_replace(array_keys($configContents), array_values($configContents), $templateContents);
